-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 03 Décembre 2013 à 20:24
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `jcsi`
--
CREATE DATABASE IF NOT EXISTS `jcsi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `jcsi`;

-- --------------------------------------------------------

--
-- Structure de la table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `client_id` (`clientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `address`
--

INSERT INTO `address` (`id`, `clientId`, `address1`, `address2`, `city`, `state`, `zipCode`) VALUES
(1, 1, '103, rue de jesaispas', NULL, 'Lyon', 'France', '69003'),
(2, 2, '56 rue dupouet', NULL, 'Santo Domingo', 'Republica Dominicana', '4922'),
(3, 3, '90, boulevard Vivier-Merle', NULL, 'Lyon', 'France', '69003'),
(4, 4, '16, rue Lavoisier', NULL, 'Lyon', 'France', '69003');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `lastName` varchar(64) NOT NULL,
  `firstName` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phoneNumber` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `grade` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `login`, `lastName`, `firstName`, `email`, `phoneNumber`, `password`, `grade`) VALUES
(1, 'mart_u', 'Martin', 'Thomas', 'thomas.martin@epitech.eu', '0123456978', 'qwerty', 0),
(2, 'miaou_j', 'Miaou', 'John', 'mia.ou@waouf.fr', '0269542514', '123456789', 0),
(3, 'virice_k', 'Viricel', 'Kevin', 'virice_k@epitech.eu', '0625346498', 'qwerty', 10),
(4, 'ville_m', 'Ville', 'Mathieu', 'ville_m@epitech.eu', '0629346767', 'qwerty', 10);

-- --------------------------------------------------------

--
-- Structure de la table `client_order`
--

CREATE TABLE IF NOT EXISTS `client_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `orderLineId` int(11) NOT NULL,
  `status` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clientId` (`orderLineId`),
  KEY `clientId_2` (`clientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=6 ;

--
-- Contenu de la table `client_order`
--

INSERT INTO `client_order` (`id`, `clientId`, `orderLineId`, `status`) VALUES
(3, 1, 1, 'payed'),
(4, 3, 2, 'Waiting for the stock'),
(5, 4, 3, 'delivered');

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL COMMENT 'percent',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `item`
--

INSERT INTO `item` (`id`, `name`, `description`, `price`, `discount`) VALUES
(1, 'Nerf Gun', 'Plastic weapon shooting foam bullets. Sold with 10 bullets.', 12.25, 0),
(2, 'Chief gift', 'Mysterious item. You will love it!', 20, 10),
(3, 'Sweet candies', 'Candies with sugar made with love.', 1.7, 0),
(4, '6 Pack Beer XXXX', 'The famous Australian beer', 10.85, 10),
(5, 'Boxing gloves', 'Gloves to do boxe', 60.7, 0),
(6, 'Pound of Butter', 'The best farmer butter of the country! Dealed fresh.', 2.36, 0),
(7, 'Ton of coal', 'The very rare coal of St Etienne. Exported by the colliers themselves.', 1350.99, 0);

-- --------------------------------------------------------

--
-- Structure de la table `order_line`
--

CREATE TABLE IF NOT EXISTS `order_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `itemId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemId` (`itemId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `order_line`
--

INSERT INTO `order_line` (`id`, `date`, `itemId`, `quantity`, `price`) VALUES
(1, '2013-11-28', 3, 1, 1.7),
(2, '2013-11-13', 7, 2, 1350.99),
(3, '2013-11-28', 4, 2, 10.85);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `client_order`
--
ALTER TABLE `client_order`
  ADD CONSTRAINT `client_order_ibfk_3` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `client_order_ibfk_1` FOREIGN KEY (`orderLineId`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `client_order_ibfk_2` FOREIGN KEY (`orderLineId`) REFERENCES `order_line` (`id`);

--
-- Contraintes pour la table `order_line`
--
ALTER TABLE `order_line`
  ADD CONSTRAINT `order_line_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `item` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
