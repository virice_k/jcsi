package com.epitech.jcsi.dbbeans;

import java.util.Date;

public class OrderLine implements Entity {

	private long		id;
	private Item		item;
	private Date		date;
	private int			quantity;
	private float		price;
	
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public long getId() {
		return id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		String ret = new String();
		ret = "ORDERLINE toString\n";
		ret += "id : " + this.getId() + "\n";
		ret += "Item : \n" + (this.getItem());
		ret += "date : " + this.getDate() + "\n";
		ret += "quantity : " + this.getQuantity() + "\n";
		ret += "price : " + this.getPrice();
		return ret;
	}
	
	public OrderLine(Item item, long id, Date date, int quantity, float price) {
		this.setItem(item);
		this.setId(id);
		this.setDate(date);
		this.setQuantity(quantity);
		this.setPrice(price);
	}
	
}
