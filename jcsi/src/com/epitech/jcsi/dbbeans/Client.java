package com.epitech.jcsi.dbbeans;
import java.util.List;

public class Client implements Entity {
	private long			id;
	private String			login;
	private String			lastName;
	private String			firstName;
	private String			email;
	private String			phoneNumber;
	private String			password;
	private int				grade;
	private List<Address>	addressList;
	private	ClientOrder		clientOrder;
	
	public long getId() {
		return id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public List<Address> getAddressList() {
		return addressList;
	}
	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}
	public ClientOrder getClientOrder() {
		return clientOrder;
	}
	public void setClientOrder(ClientOrder clientOrder) {
		this.clientOrder = clientOrder;
	}
	
	@Override
	public String toString() {
		String ret = new String();
		ret = "id : " + this.getId() + "\n";
		ret += "login : " + this.getLogin() + "\n";
		ret += "lastName : " + this.getLastName() + "\n";
		ret += "firstName : " + this.getFirstName() + "\n";
		ret += "email : " + this.getEmail() + "\n";
		ret += "phoneNumber : " + this.getPhoneNumber() + "\n";
		ret += "password : " + this.getPassword() + "\n";
		ret += "grade : " + this.getGrade() + "\n";
		ret += "addressList : \n";
		for (Address address : this.getAddressList()) {
			ret += "   " + address + "\n";
		}
		return ret;
	}
	
	public Client() {
		
	}
	
	public Client(long id, String login, String lastName, String firstName, String email, String phoneNumber,
			String password, int grade, List<Address> addressList, ClientOrder clientOrder) {
		this.setId(id);
		this.setEmail(email);
		this.setLogin(login);
		this.setPassword(password);
		this.setGrade(grade);
		this.setPhoneNumber(phoneNumber);
		this.setClientOrder(clientOrder);
		this.setLastName(lastName);
		this.setFirstName(firstName);
		this.setAddressList(addressList);
	}	
}