package com.epitech.jcsi.dbbeans;

import java.util.List;

public class ClientOrder implements Entity {
	private long				id;
	private List<OrderLine> 	orderLineList;
	private String				status;
	
	public long getId() {
		return id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
	}
	public List<OrderLine> getOrderLineList() {
		return orderLineList;
	}
	public void setOrderLineList(List<OrderLine> itemList) {
		this.orderLineList = itemList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		String ret = new String();
		ret = "   orderLineList : \n";
		for (OrderLine ol : orderLineList) {
			ret += "      " + ol + "\n";
		}
		ret += "status : " + this.getStatus() + "\n";
		return ret;
	}

	public ClientOrder() {
		
	}
	
	public ClientOrder(List<OrderLine> orderLineList, String status) {
		this.setOrderLineList(orderLineList);
		this.setStatus(status);
	}

}
