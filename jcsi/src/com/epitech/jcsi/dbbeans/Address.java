package com.epitech.jcsi.dbbeans;

public class Address implements Entity {
	private long id;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	
	public long getId() {
		return id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		String ret = new String();
		
		ret = "id : " + this.getId() + "\n";
		ret += "address1 : " + this.getAddress1() + "\n";
		ret += "address2 : " + this.getAddress2() + "\n";
		ret += "city : " + this.getCity() + "\n";
		ret += "state : " + this.getState() + "\n";
		ret += "zipCode : " + this.getZipCode() + "\n";
		return ret;
	}

	public Address() {

	}
	
	public Address(long id, String address1, String address2, String city, String state, String zipCode){
		this.setId(id);
		this.setAddress1(address1);
		this.setAddress2(address2);
		this.setCity(city);
		this.setState(state);
		this.setZipCode(zipCode);
	}
}
