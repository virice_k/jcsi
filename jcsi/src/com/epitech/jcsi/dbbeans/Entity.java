package com.epitech.jcsi.dbbeans;

public interface Entity {
	void setId(long id);
	long getId();
}
