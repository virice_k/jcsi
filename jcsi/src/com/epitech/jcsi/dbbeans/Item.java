package com.epitech.jcsi.dbbeans;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class Item implements Entity {
	private long		id;
	private String		name;
	private String		description;
	private float		price;
	private float		discount;
	
	public long	getId() {
		return id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(Float discount) {
		this.discount = discount;
	}
	
	public Item() {
		
	}
	
	public Item(String name, String description, float price, float discount) {
		this.setName(name);
		this.setDescription(description);
		this.setDiscount(discount);
		this.setPrice(price);
	}
	
	@Override
	public String toString() {
		String ret = new String();
		Method[] methods = this.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method m = methods[i];
			String methodName = m.getName();
			if (methodName.startsWith("get")
					&& !methodName.equals("getClass")
					&& m.getParameterTypes().length == 0) {
				Object value = null;
				try {
					value = m.invoke(this, (Object[])(null));
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String name = methodName.substring("get".length());
				name = Character.toLowerCase(name.charAt(0)) + name.substring(1);
				ret += name + ": " + value + "\n";
			}
		}
		return ret;
	}
}
