package com.epitech.jcsi.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnection {

	private static String url = "jdbc:mysql://localhost:3306/jcsi";
	private static String user = "root";
	private static String pwd = "";
	private static Connection connect = null;
	
	/**
	 * get the singleton instance
	 * @return connection instance
	 */
	public static Connection getInstance() {
		if (connect == null) {		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		try {
			connect = DriverManager.getConnection(url, user, pwd);
		}
		catch (SQLException e) {
			e.printStackTrace();	
		}
		}
		return connect;
	}
}