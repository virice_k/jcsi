package com.epitech.jcsi;

import com.epitech.jcsi.dbbeans.Item;
import com.epitech.jcsi.persistencelayer.PersitenceLayer;

public class Jcsi {
	public static void main(String[] args) {
		Item i = new Item("quenelle", "real quenelle from Lyon", 1337, 0);
		i.setId(5);
		PersitenceLayer p = new PersitenceLayer();
		//p.save(i);
		//p.remove(i);
		i = (Item) p.read(Item.class, 6);
		//p.update(i);
		System.out.println(i);
	}
}
