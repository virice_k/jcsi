package com.epitech.jcsi.persistencelayer;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.epitech.jcsi.configuration.MappingInformation;
import com.epitech.jcsi.dbbeans.Entity;
import com.epitech.jcsi.dbconnection.MySqlConnection;

public class PersitenceLayer {
	private MappingInformation mi = new MappingInformation("conf.xml");
	private Connection conn = MySqlConnection.getInstance();

	private Entity executeSqlRequest(String sql, Entity e, int nbProperty,
			List<String> propertyNames) {
		try (PreparedStatement ps = conn.prepareStatement(sql,
				Statement.RETURN_GENERATED_KEYS);) {

			for (int i = 0; i < nbProperty; i++) {
				String prop = propertyNames.get(i);
				try {
					Method m = e.getClass().getMethod(
							"get" + Character.toUpperCase(prop.charAt(0))
									+ prop.substring(1));
					ps.setObject(i + 1, m.invoke(e, (Object[]) (null)));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.first())
				e.setId(rs.getLong(1));
		} catch (SQLException exc) {
			exc.printStackTrace();
		}
		return e;
	}

	public Entity save(Entity e) {
		StringBuilder sql = new StringBuilder();
		List<String> propertyNames = new ArrayList<String>();

		int nbProperty = 0;
		List<String> properties;
		String className = e.getClass().getSimpleName();
		properties = mi.getClassMapProperties(Character.toUpperCase(className
				.charAt(0)) + className.substring(1));

		sql.append("INSERT INTO ");
		sql.append(Character.toLowerCase(className.charAt(0))
				+ className.substring(1));
		sql.append(" (");

		Method[] methods = e.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method m = methods[i];
			String methodName = m.getName();
			if (methodName.startsWith("get") && !methodName.equals("getClass")
					&& m.getParameterTypes().length == 0) {
				String name = methodName.substring("get".length());
				name = Character.toLowerCase(name.charAt(0))
						+ name.substring(1);
				if (properties.contains(name)) {
					propertyNames.add(name);
					nbProperty++;
				}
			}
		}

		for (int i = 0; i < nbProperty; i++) {
			sql.append(propertyNames.get(i));
			if (i + 1 < nbProperty)
				sql.append(",");
		}

		sql.append(") VALUES (");

		for (int i = 0; i < nbProperty; i++) {
			sql.append("?");
			if (i + 1 < nbProperty)
				sql.append(",");
		}
		sql.append(")");

		e = executeSqlRequest(sql.toString(), e, nbProperty, propertyNames);
		return e;
	}

	public Entity read(Class<? extends Entity> clazz, long id) {
		StringBuilder sql = new StringBuilder();
		try {
			Entity e = clazz.newInstance();
			String className = e.getClass().getSimpleName();
			sql.append("SELECT * FROM ");
			sql.append(Character.toLowerCase(className.charAt(0))
					+ className.substring(1));
			sql.append(" WHERE `id` = ?");

			try (PreparedStatement ps = conn.prepareStatement(sql.toString());) {
				ps.setLong(1, id);
				ResultSet rs = ps.executeQuery();

				List<String> properties = mi.getClassMapProperties(Character
						.toUpperCase(className.charAt(0))
						+ className.substring(1));
				if (!rs.first())
					return null;
				for (String s : properties) {
					Object property = rs.getObject(s);
					Class<?>[] cArg = new Class[1];
					cArg[0] = property.getClass();
					Method m = e.getClass().getMethod(
							"set" + Character.toUpperCase(s.charAt(0))
									+ s.substring(1), cArg);
					m.invoke(e, property);
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.setId(id);
			return e;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public Entity update(Entity e) {
		String className = e.getClass().getSimpleName();
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(Character.toLowerCase(className.charAt(0))
				+ className.substring(1));
		sb.append(" SET ");
		List<String> properties = mi.getClassMapProperties(Character
				.toUpperCase(className.charAt(0)) + className.substring(1));
		for (ListIterator<String> itr = properties.listIterator(); itr
				.hasNext();) {
			String s = itr.next();
			sb.append(s);
			sb.append(" = ");
			sb.append("?");
			if (itr.hasNext())
				sb.append(", ");
		}
		sb.append(" WHERE `id` = ?");
		int i = 1;
		try (PreparedStatement ps = conn.prepareStatement(sb.toString());) {
			for (ListIterator<String> itr = properties.listIterator(); itr
					.hasNext(); i++) {
				String s = itr.next();
				Method m = e.getClass().getMethod(
						"get" + Character.toUpperCase(s.charAt(0))
								+ s.substring(1), (Class[]) (null));
				Object obj = m.invoke(e, (Object[]) (null));
				ps.setObject(i, obj);
			}
			ps.setObject(i, e.getId());
			ps.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return e;
	}

	public void remove(Entity e) {
		String className = e.getClass().getSimpleName();
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(Character.toLowerCase(className.charAt(0))
				+ className.substring(1));
		sb.append(" WHERE `id` = ?");
		try (PreparedStatement ps = conn.prepareStatement(sb.toString());) {
			ps.setLong(1, e.getId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}