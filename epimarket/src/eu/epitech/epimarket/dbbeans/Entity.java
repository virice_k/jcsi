package eu.epitech.epimarket.dbbeans;

public interface Entity {
	void setId(long id);
	long getId();
}
