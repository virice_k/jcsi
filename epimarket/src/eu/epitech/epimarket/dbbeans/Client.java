package eu.epitech.epimarket.dbbeans;

public class Client implements Entity {

	private long			id;
	private String			login;
	private String			lastName;
	private String			firstName;
	private String			email;
	private String			phoneNumber;
	private String			password;
	private int				grade;
	private String			address;
	private String			city;
	private String			zipcode;

	public Client(long id, String login, String lastName, String firstName,
			String email, String phoneNumber, String password, int grade,
			String address, String city, String zipcode) {
		super();
		this.id = id;
		this.login = login;
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.grade = grade;
		this.address = address;
		this.city = city;
		this.zipcode = zipcode;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Client newInstance() {
		return (new Client(0, null, null, null,
				null, null, null, 0,
				null, null, null));
	}

	
}
