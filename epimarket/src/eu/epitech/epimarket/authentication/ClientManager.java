package eu.epitech.epimarket.authentication;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import eu.epitech.epimarket.dbbeans.Client;
import eu.epitech.epimarket.dbbeans.ClientOrder;
import eu.epitech.epimarket.dbbeans.Item;
import eu.epitech.epimarket.persistencelayer.*;

@ManagedBean(name="client")
@SessionScoped
public class ClientManager {

	private long			id;
	private String			login;
	private String			lastName;
	private String			firstName;
	private String			email;
	private String			phoneNumber;
	private String			password;
	private int				grade;
	private String			address;
	private String			city;
	private String			zip;
	private Client			clientEntity;
	private	ClientOrder		clientOrder;
	@ManagedProperty("#{cart}")
	private CartManager		cart;
	
	public boolean	  loadFromDB() {
		
		PersitenceLayer	persitence = new PersitenceLayer();
		
		//persitence.readClient(clientEntity, login);
		return (true);
	}

	
	
	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getZip() {
		return zip;
	}



	public void setZip(String zip) {
		this.zip = zip;
	}



	public Client getClientEntity() {
		return clientEntity;
	}



	public void setClientEntity(Client clientEntity) {
		this.clientEntity = clientEntity;
	}



	public long getId() {
		return id;
	}
	
	public void addToCart(Item item) {
		clientOrder.addItem(item);
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ClientOrder getClientOrder() {
		return clientOrder;
	}

	public void setClientOrder(ClientOrder clientOrder) {
		this.clientOrder = clientOrder;
	}

	public CartManager getCart() {
		return cart;
	}

	public void setCart(CartManager cart) {
		this.cart = cart;
	}		
	
}
