package eu.epitech.epimarket.authentication;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import eu.epitech.epimarket.dbbeans.Item;
import eu.epitech.epimarket.jcsi.Jcsi;
import eu.epitech.epimarket.persistencelayer.PersitenceLayer;

@ManagedBean(name="catalog")
@SessionScoped
public class Catalog {

	private	List<Item>		itemsList = new ArrayList<Item>();
	private Item			current = null;
	

	public Catalog() {
		//Jcsi		jcsi = new Jcsi();
		PersitenceLayer		persitence = new PersitenceLayer();

		//List<Item> test = persitence.readItemsList();
		//this.itemsList = persitence.readItemsList();
		addItem(new Item(1,"Bo�te de chocolats", "Chocolats re�us � No�l qui n'ont pas �t� mang�s", 5.0f, 0f));
		addItem(new Item(2,"Chaussures trou�es", "Elles prennent l'eau, mais elles sont utiles quand il fait beau!", 25.0f, 0f));
		addItem(new Item(3,"Fourchette sans dents", "Sert aussi de couteau si on l'utilise bien!", 2.6f, 0f));
		addItem(new Item(4,"Piles us�es", "Nouveau jeu de construction pour les enfants, et moins cher!", 1.75f, 0f));
		addItem(new Item(5,"Vraie fausse Porsche Cayenne en miniature", "Comme le poivre!", 26.72f, 0f));
		addItem(new Item(6,"CD ray� de Justin Bieber", "Plus besoin de vous �nerver, il est d�j� ray�!", 25.0f, 0f));
	//System.out.println(test);
	}
	
	public List<Item> getItemsList() {
		return itemsList;
	}

	public void setItemsList(List<Item> itemList) {
		this.itemsList = itemList;
	}
	
	public void	addItem(Item item) {
		itemsList.add(item);
	}
	
	public boolean removeItem(Item item) {
		return (itemsList.remove(item));
	}

	public Item getCurrent() {
		return current;
	}

	public void setCurrent(Item current) {
		this.current = current;
	}
	
	public static void main(String[] args) {
		PersitenceLayer		tmp = new PersitenceLayer();
		List<Item>			test = tmp.readItemsList();
		
		for (Iterator<Item> iterator = test.iterator(); iterator.hasNext();) {
			Item itm = (Item) iterator.next();
			System.out.println("Name: " + itm.getName() + "  Description: " + itm.getDescription() + "   Price: " + itm.getPrice());
			
			
		}
	}
	
	
}
