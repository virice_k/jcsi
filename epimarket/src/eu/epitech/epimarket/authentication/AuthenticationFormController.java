package eu.epitech.epimarket.authentication;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="authenticationForm")
@RequestScoped
public class AuthenticationFormController {

	@ManagedProperty("#{client}")
	private ClientManager client;
	private String login;
	private String password;
	private boolean alreadyTried;

	public boolean areGoodCredentials(String login, String password) {
		if (login != null &&
			login.equals("toto") &&
			password != null &&
			password.equals("test"))
			return true;
		else
			return false;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAlreadyTried() {
		return alreadyTried;
	}

	public void setAlreadyTried(boolean alreadyTried) {
		this.alreadyTried = alreadyTried;
	}
	
	public ClientManager getClient() {
		return client;
	}

	public void setClient(ClientManager client) {
		this.client = client;
	}
	
	@ManagedProperty("#{logout}")
	public String logout() {
		client.setLogin("");
		alreadyTried = false;
		client.getCart().delCart();
		return "authenticate";
}
	
	@ManagedProperty("#{authenticate}")
	public String authenticate() {
		if (areGoodCredentials(login, password)) {
			client.setLogin(login);
			alreadyTried = false;
			return "shop";
		}
		else {
			alreadyTried = true;
			return "authenticate";
		}
	}
	
}

