package eu.epitech.epimarket.authentication;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import eu.epitech.epimarket.dbbeans.Item;
import eu.epitech.epimarket.dbbeans.OrderLine;

@ManagedBean(name = "cart")
@SessionScoped
public class CartManager {

    private List<OrderLine> orderLines = new ArrayList<>();
    private Item currentItem;
    private Float totalPrice = new Float(0);
    private boolean areItemsInCart;  
    
   public void delCart() {
	   orderLines.clear();
	   areItemsInCart = false;
	   totalPrice = 0f;
   }
    
    public String addItemFromCartPage() {
    	addItem();
        return "cart";
    }
    
    public String addItemFromShopPage() {
    	addItem();
        return "shop";
    }
    
    public void addItem() {
        boolean found = false;
        for (Iterator<OrderLine> iter = orderLines.iterator(); iter.hasNext() && !found;) {
            OrderLine currentOL = iter.next();
            if (currentOL.getItem().getId() == currentItem.getId()) {
                currentOL.increment();
                currentOL.setPrice(currentOL.getPrice() + currentItem.getPrice());
                found = true;
            }
        }
        if (!found) {
            OrderLine ol = new OrderLine(currentItem, 0, new Date(), 1, currentItem.getPrice());
            orderLines.add(ol);
        }
        areItemsInCart = true;
        totalPrice += currentItem.getPrice();
    }
    
    public String delItem() {
    	boolean end = false;
        for (Iterator<OrderLine> iter = orderLines.iterator(); iter.hasNext() && !end;) {
            OrderLine currentOL = iter.next();
            if (currentOL.getItem().getId() == currentItem.getId()) {
                currentOL.decrement();
               currentOL.setPrice(currentOL.getPrice() - currentItem.getPrice());
                if (currentOL.getQuantity() == 0) {
                	orderLines.remove(currentOL);
                	end = true;
                }
            }
            if (orderLines.isEmpty()) {
            	areItemsInCart = false;
            }
        }
       totalPrice -= currentItem.getPrice();
        return "cart";
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public Item getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(Item selectedItem) {
        this.currentItem = selectedItem;
    }
	public Float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public boolean isAreItemsInCart() {
		return areItemsInCart;
	}
	public void setAreItemsInCart(boolean areItemsInCart) {
		this.areItemsInCart = areItemsInCart;
	}
	

    
}
