package eu.epitech.epimarket.authentication;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="createMemberForm")
@RequestScoped
public class CreateMemberFormController {

	private String login;
	private String password;
	private String email;
	private String address;
	private String zip;
	private String city;
	private String lastName;
	private String firstName;
	private String alertMsg;

	public boolean	addMember() {

		// Add member in DB
		// if error -> return (false);
		//else
		return (true);
	}
	
	public boolean  isUserAlreadyInDB() {
		// Check in the DB if "login" already exists.
		return (false);
	}
	
	public String validNewMember() {
		if (login != null &&
			password != null &&
			email != null &&
			address != null &&
			zip != null &&
			lastName != null &&
			firstName != null &&
			city != null) {
			if (isUserAlreadyInDB())
				alertMsg = "Error: Member already exists.\n";
			else
				validNewMember();
			alertMsg = "Congratulations! Your account has been created!";
		}
		else
			alertMsg = "Error: Your credentials are not compliant.\n";
		return ("createmember");
	}

	public String getAlertMsg() {
		return alertMsg;
	}

	public void setAlertMsg(String alertMsg) {
		this.alertMsg = alertMsg;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}



	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}



	public String getZip() {
		return zip;
	}


	public void setZip(String zip) {
		this.zip = zip;
	}


	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
}

