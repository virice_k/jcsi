package eu.epitech.epimarket.jcsi;

import java.util.Iterator;
import java.util.List;

import eu.epitech.epimarket.dbbeans.Client;
import eu.epitech.epimarket.dbbeans.Item;
import eu.epitech.epimarket.persistencelayer.PersitenceLayer;








public class Jcsi {
	
	
	public List<Item> itemList() {
		PersitenceLayer p = new PersitenceLayer();
		List<Item> test = p.readItemsList();
		return test;
	}
	
	public static void main(String[] args) {
	

		Jcsi		jcsi = new Jcsi();
		Item i = new Item();
		i.setId(5);
		
		//p.save(i);
		//p.remove(i);
		//i = (Item) p.read(Item.class, 1);
		List<Item>	test = jcsi.itemList();
		//p.update(i);
		for (Iterator<Item> iterator = test.iterator(); iterator.hasNext();) {
			Item itm = (Item) iterator.next();
			System.out.println("Name: " + itm.getName() + "  Description: " + itm.getDescription() + "   Price: " + itm.getPrice());
			
			
		}
	}
}
