package eu.epitech.epimarket.configuration;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class MappingInformation {
	private File file;
	private DocumentBuilder builder;
	private Document doc;

	public MappingInformation(String mapFile) {
			InputStream is = MappingInformation.class.getResourceAsStream(mapFile);
			System.out.println("before");
			try {
			// file = new File(mapFile);
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			System.out.println("after1");

			doc = builder.parse(is);
			System.out.println("after2");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Node getClassNode(Node n, String className) {
		short nodeType = n.getNodeType();
		if (nodeType == Node.ELEMENT_NODE) {
			if (n.getNodeName().equals("class")) {
				NamedNodeMap attrs = n.getAttributes();
				for (int i = 0; i < attrs.getLength(); i++) {
					Node attr = attrs.item(i);
					if (attr.getNodeName().equals("name")
							&& attr.getNodeValue().equals(className)) {
						return n;
					}
				}
			}
		}

		NodeList list = n.getChildNodes();
		for (int i = 0; i < list.getLength(); i++) {
			Node child = list.item(i);
			n = getClassNode(child, className);
			if (n.getNodeName().equals("class")) {
				return n;
			}
		}
		return n;
	}

	private List<String> getClassNodeProperties(Node classNode,
			List<String> properties) {
		NodeList list = classNode.getChildNodes();
		for (int i = 0; i < list.getLength(); i++) {
			Node child = list.item(i);
			short nodeType = child.getNodeType();
			if (nodeType == Node.ELEMENT_NODE
					&& child.getNodeName().equals("property")) {
				NamedNodeMap attrs = child.getAttributes();
				for (int j = 0; j < attrs.getLength(); j++) {
					Node attr = attrs.item(j);
					if (attr.getNodeName().equals("name"))
						properties.add(attr.getNodeValue());
				}
			}
		}
		return properties;
	}

	public List<String> getClassMapProperties(String className) {
		List<String> properties = new ArrayList<>();
		Node n = this.doc;

		n = getClassNode(n, className);
		properties = getClassNodeProperties(n, properties);
		return properties;
	}

	public List<CollectionInformation> getClassMapList(String className) {
		List<CollectionInformation> properties = new ArrayList<>();
		Node n = getClassNode(this.doc, className);
		
		
		return properties;
	}
}